
#ifndef NST_SOCK_UNIX_H
#define NST_SOCK_UNIX_H 1

#include <nebase/cdefs.h>

/**
 * \param[out] in_use will be set if really in use
 * \param[out] type will be set to real type if in use, and 0 if not
 * \return  0 if check ok, values will be set in out params
 *         -1 if check failed
 *          1 if check is not suported
 */
extern int nst_sock_unix_path_in_use(const char *path, int *in_use, int *type)
	_nattr_nonnull((1, 2, 3));

#endif
