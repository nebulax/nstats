
#include <nebase/osinfo.h>
#include <nebase/syslog.h>

#if defined(OS_LINUX)
# include "platform/linux.h"
#elif defined(OS_FREEBSD)
# include "platform/freebsd.h"
#elif defined(OS_NETBSD)
# include "platform/netbsd.h"
#elif defined(OS_DFLYBSD)
# include "platform/dflybsd.h"
#elif defined(OS_SOLARIS)
# include "platform/solaris.h"
#elif defined(OS_ILLUMOS)
# include "platform/illumos.h"
#elif defined(OS_OPENBSD)
# include "platform/openbsd.h"
#elif defined(OS_HAIKU)
//NOTE no unix path check
#elif defined(OS_DARWIN)
# include "platform/xnu.h"
#else
# error "fix me"
#endif

int nst_sock_unix_path_in_use(const char *path, int *in_use, int *type)
{
	int ret = 0;
	*in_use = 0;
	*type = 0;
#if defined(OS_LINUX)
	neb_ino_t fs_ni;
	if (neb_file_get_ino(path, &fs_ni) != 0) {
		neb_syslog(LOG_ERR, "Failed to get ino for %s", path);
		return -1;
	}
	ino_t sock_ino = 0;
	if (nst_sock_unix_get_ino(&fs_ni, &sock_ino, type) != 0)
		ret = -1;
	if (sock_ino)
		*in_use = 1;
#elif defined(OS_FREEBSD)
	kvaddr_t sockptr = 0;
	if (nst_sock_unix_get_sockptr(path, &sockptr, type) != 0)
		ret = -1;
	if (sockptr)
		*in_use = 1;
#elif defined(OS_NETBSD)
	uint64_t sockptr = 0;
	if (nst_sock_unix_get_sockptr(path, &sockptr, type) != 0)
		ret = -1;
	if (sockptr)
		*in_use = 1;
#elif defined(OS_DFLYBSD)
	void *sockptr = NULL;
	if (nst_sock_unix_get_sockptr(path, &sockptr, type) != 0)
		ret = -1;
	if (sockptr)
		*in_use = 1;
#elif defined(OS_OPENBSD)
	uint64_t sockptr = 0;
	if (nst_sock_unix_get_sockptr(path, &sockptr, type) != 0)
		ret = -1;
	if (sockptr)
		*in_use = 1;
#elif defined(OSTYPE_SUN)
	uint64_t sockptr = 0;
	if (nst_sock_unix_get_sockptr(path, &sockptr, type) != 0)
		ret = -1;
	if (sockptr)
		*in_use = 1;
#elif defined(OS_DARWIN)
	so_type_t sockptr = (so_type_t)0;
	if (nst_sock_unix_get_sockptr(path, &sockptr, type) != 0)
		ret = -1;
	if (sockptr)
		*in_use = 1;
#else
	neb_syslog(LOG_INFO, "Unix sock path check is not available in this platform");
	ret = 1;
#endif
	if (ret < 0)
		neb_syslog(LOG_ERR, "Failed to check if %s is in use", path);
	return ret;
}
