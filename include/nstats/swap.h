
#ifndef NST_SWAP_H
#define NST_SWAP_H 1

#include <nebase/cdefs.h>

#include <sys/types.h>

struct nst_swap;
typedef struct nst_swap* nst_swap_t;

extern nst_swap_t nst_swap_load(void)
	_nattr_warn_unused_result;
extern void nst_swap_release(nst_swap_t s)
	_nattr_nonnull((1));

extern int nst_swap_device_num(const nst_swap_t s)
	_nattr_nonnull((1)) _nattr_pure;

typedef void (* swap_device_each_t)(const char *filename, size_t total, size_t used, void *udata);
extern void nst_swap_device_foreach(const nst_swap_t s, swap_device_each_t f, void *udata)
	_nattr_nonnull((1, 2));

#endif
