
#include <nstats/swap.h>

#include "main.h"

#include <stdio.h>

static void print_swap(const char *filename, size_t total, size_t used, void *udata _nattr_unused)
{
	fprintf(stdout, "%s: total %zu, used %zu\n", filename, total, used);
}

void print_stats_swap(void)
{
	nst_swap_t s = nst_swap_load();
	if (!s) {
		fprintf(stderr, "failed to get swap stats\n");
		return;
	}

	nst_swap_device_foreach(s, print_swap, NULL);

	nst_swap_release(s);
}
