
#include "version.h"

static const char nst_version[] = NSTATS_VERSION_STRING;

const char *nst_version_str(void)
{
	return nst_version;
}

int nst_version_code(void)
{
	return NSTATS_VERSON_CODE;
}
