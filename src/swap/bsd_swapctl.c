
#include <nebase/syslog.h>

#include <nstats/swap.h>

#include <sys/types.h>
#include <sys/swap.h>
#include <sys/stat.h> // for S_BLKSIZE
#include <unistd.h>
#include <stdlib.h>

struct nst_swap {
	struct swapent *entl;
	int count;
};

nst_swap_t nst_swap_load(void)
{
	int nswap = swapctl(SWAP_NSWAP, NULL, 0);
	if (nswap == -1) {
		neb_syslogl(LOG_ERR, "swapctl: %m");
		return NULL;
	}

	struct nst_swap *s = malloc(sizeof(struct nst_swap));
	if (!s) {
		neb_syslogl(LOG_ERR, "malloc: %m");
		return NULL;
	}
	s->count = 0;
	s->entl = NULL;
	if (!nswap) // no swap found
		return s;

	s->entl = malloc(nswap * sizeof(struct swapent));
	if (!s->entl) {
		neb_syslogl(LOG_ERR, "malloc: %m");
		nst_swap_release(s);
		return NULL;
	}

	s->count = swapctl(SWAP_STATS, s->entl, nswap);
	if (s->count == -1) {
		neb_syslogl(LOG_ERR, "swapctl: %m");
		nst_swap_release(s);
		return NULL;
	}

	return s;
}

void nst_swap_release(nst_swap_t s)
{
	if (s->entl)
		free(s->entl);
	free(s);
}

int nst_swap_device_num(const nst_swap_t s)
{
	return s->count;
}

void nst_swap_device_foreach(const nst_swap_t s, swap_device_each_t f, void *udata)
{
	for (int i = 0; i < s->count; i++) {
		struct swapent *ent = s->entl + i;
		f(ent->se_path, ent->se_nblks * S_BLKSIZE, ent->se_inuse * S_BLKSIZE, udata);
	}
}
